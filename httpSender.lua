-- httpSender.lua

-- send a dataset to server
function sendDataSet(timeStamp)

-- search for file
if file.open(timeStamp .. ".json" , "r") then 
     text = tostring(file.read()) -- read file, convert to string

    if AES_key ~= nil then
        iv = makeString(16) --genareate IV
        
    -- encrypt the body with AES-CBC
      encoded = crypto.toBase64(crypto.encrypt("AES-CBC", AES_key, text,iv))
        print("encoded data: " .. encoded)
         -- send over http
         http.put("http://".. Server.. ":" .. HTTP,
         "Content-Type: application/octet-stream\r\n"
        ,iv.."~" ..  encoded, function(code,data) 
        print(code .. " OK")
        end)
    end
   file.close()   
end
end

-- get the key over HTTPS 
function getKey(DeviceID) 

conn = net.createConnection(net.TCP,1)
conn:connect(HTTPS,Server)
-- if connected, send a PUT request
conn:on("connection", function(sck, c)
   sck:send("PUT /".. DeviceID .. " HTTPS/1.1\r\nHost:" .. 
   Server .. ":" .. HTTPS .. "\r\nUser-Agent: INSUPUMP/".. Version  .. "\r\n\r\n")
end)

return conn:on("receive", function(sck, c)
	-- check if it is the payload, instead of header
    if stringStart(c,"HTTP") == false then 
		-- check if a update file is attached
        if string.len(c) > 16 then 
 
			-- get key, in front of payload
            AES_key = string.sub(c, 0 ,16)
            
			-- update at the back
            update = string.sub(c, 17 ,-1)
			
			-- get file name, with splitter /, returns character position 
            filenameEnd = (string.find(update,"//"))
			
			-- get version, with splitter \, returns character position
            versionNumberEnd = (string.find(update,"\\"))

            hashEnd = (string.find(update,"~"))

                -- check if the addtional info is present
                if filenameEnd ~= nil and versionNumberEnd ~= nil and hash ~= nil then 
					-- read filename itself
                    filename = string.sub(update, 4 ,(filenameEnd - 1))

                    -- read hash of file 
                    hash = string.sub(update,(versionNumberEnd+2), (hashEnd-1))

                    -- compare the hash 
                    -- if hash == crypto.hash("sha256", update) then 
					    -- write to file
                        if file.open(filename, "w") then
                            file.write(update)
                            file.close()

						    -- update version
                            Version = string.sub(update,(filenameEnd + 2),(versionNumberEnd - 1))
						
						    -- logging
                            print("INSUPUMP Updated: " .. filename .. " to Version: " .. Version)
                        end
                   -- end
               end
            else

		-- if there is no update
        print("no update")
        AES_key = c 
        end
		
		-- start the procces of sending data
    tmr.alarm(6, 10000, tmr.ALARM_AUTO, function()
        -- todo sloop bloodlevel eruit
        sendDataSet(addSensorData(math.random(3,8)))
    end)
      -- sck:close() gives kernel errors on crypto:)
      end
    end)
end
