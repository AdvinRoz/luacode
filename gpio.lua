-- gpio.lua

local buttonPin = 5
lastlogin = tmr.now() - 10000000

function debounce()
    -- don't react to any interupts from now on and wait 50ms until the interrupt for the up event is enabled
    -- within that 50ms the switch may bounce to its heart's content
    gpio.trig(buttonPin, "none")
    tmr.alarm(3, 50, tmr.ALARM_SINGLE, function()
        gpio.trig(buttonPin, "up", debounce)
    end)
    sendLogin()
    -- finally react to the down event
end

gpio.mode(buttonPin, gpio.INT,gpio.PULLUP)
gpio.trig(buttonPin, 'up', debounce)

function blinkLED(pin, delay) 
-- assign status of pin
status = true 

-- set a 250ms blink with a different delay
tmr.alarm(1,(delay*1000), tmr.ALARM_AUTO, function ()
    tmr.start(2)
    gpio.write(pin,gpio.HIGH)
    tmr.alarm(2,250,tmr.ALARM_SEMI,function()
        gpio.write(pin, gpio.LOW)
    end)
    status = not status
    end)
end

-- stop the led
function stopLED()
    tmr.unregister(1)
	tmr.unregister(2)
end
 

function sendLogin()
if lastlogin < (tmr.now() - 10000000) then
    print('to send')
    lastlogin = tmr.now()
 if AES_key ~= nil then
        iv = makeString(16) --genareate IV

        text = cjson.encode({DeviceID = DeviceID, timestamp = time})
    -- encrypt the body with AES-CBC
      encoded = crypto.toBase64(crypto.encrypt("AES-CBC", AES_key, text,iv))
        print("encoded data: " .. encoded)
         -- send over http
         http.post("http://".. Server.. ":" .. HTTP .. "/loginDevice",
         "Content-Type: application/octet-stream\r\n"
        ,iv.."~" ..  encoded, function(code,data) 
        print(code .. " OK")
        end)
    end
end
end

