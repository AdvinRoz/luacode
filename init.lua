-- init.lua 
print("----------------INSUPUMP----------------")

-- load all functions
dofile('config.lua')
dofile('util.lua')
dofile('gpio.lua')
dofile('httpSender.lua')
dofile('sensorData.lua')
blinkLED(2,1)

--init connect to AP
tmr.alarm(0, 10000, tmr.ALARM_AUTO, function()
   if wifi.sta.getip() == nil then
   wifi.setmode(wifi.STATION)
   wifi.sta.config(SSID,Network_password)
        print("Not connected to an AP...\n")
   else
       if timeCheck() == true then
            print("IP Info: " .. wifi.sta.getip())
            print('time: ' .. rtctime.get())
			blinkLED(2,5)
			-- get a key from server
            getKey(DeviceID)
            tmr.stop(0)
            end
      end
end)
