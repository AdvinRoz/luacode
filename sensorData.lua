-- sensorData.lua
local count = 1
--dat xml parser doe
function addSensorData(insulinLevel)
    if timeCheck() then
        time = rtctime.get()
        if file.open(time.. ".json", "w") then
            payload = cjson.encode({  DeviceID = DeviceID, insulinLevel = insulinLevel, timestamp = time, count = count})
            file.write(payload)
            file.close()
            count = count + 1
            return time
        end
    end
end

-- send all data to server
function sendAll()
    for name,v in pairs(file.list()) do
        if stringEnd(name, '.xml') then -- filter all xml files
         name = string.gsub(name, ".xml", "")
        timestamp = tonumber(name)
		sendDataSet(timestamp)
     end
    end
end

-- remove data from device
-- ndays = threshold for removing data, e.g 2 remove all data older then 2 days
function removeOldData(ndays)
threshold = (rtctime.get() - (86400 * ndays)) --86400 = 1 day
    for name,v in pairs(file.list()) do
        if stringEnd(name, '.json') then -- filter all json files
        timestamp = string.gsub(name, ".json", "") -- remove .json
        timestamp = tonumber(timestamp) -- convert to number
            if timestamp <= threshold then
            print("remove: " .. name)
            file.remove(name)
            end
        end
    end
end

