-- util.lua

-- get the time from a NTP server
function timeCheck()
    if rtctime.get() == 0 then
        sntp.sync(Server,    
            function(sec,usec,server)
                return true
            end,    
            function()
                print('ERROR no currentTime')
            return false
        end)
    else 
        return true
    end
end


-- check if string starts 
function stringStart(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

-- check if the string ends
function stringEnd(String,End)
   return End=='' or string.sub(String,-string.len(End))==End
end

--generate a random string
function makeString(length)
        math.randomseed(tmr.time())
        if length < 1 then return nil end -- Check for l < 1
        local s = "" -- Start string
        for i = 1, length do
                s = s .. string.char(math.random(32, 125)) -- Generate random number from 32 to 125, turn it into character and add to string
        end
        return s -- Return string
end
